#!/usr/bin/env node
const args = require("args");
const process = require("process");
const path = require("path");
const fs = require("fs");
const buffer = require("buffer");
const moment = require("moment-timezone");
const runParallel = require("run-parallel-limit");
const jsonBeautify = require("json-beautify");
const UploadYuemei = require("../plugins/uploadYuemei");
const UploadJsonQiniu = require("../plugins/uploadJson");
const getUploadPaths = require("./get_upload_paths").getUploadPaths;
const Lib = require("../lib");
let cwd = process.cwd();
console.log("cwd :", cwd);
let flags = args.parse(process.argv);
let sub = args.sub;

const PROJECT_PATH = "yaby007/ERO_PIC_PARK";
const GITEE_SESSION = "RFNBVERaZWpBSkJ4MUg1SXFLcytDNWg1SEhQL0lMVEhmeTlGeXFYcXd0THYxVUZsTllvcFFxZ0FLai9LanBIOEJkU3pETEpnYzYxUHZzM1RVNERvMVYyZ1J5MDN0SWNKM2xxSVpvN1l2OVRYbWVzek1DMlFJbTRoN0pyOExSdXMydjBlV3E1TGJvOE9DaG5VYm1wNCthNkRRNnZCUDNaLzdRLzE2bzBjUHVqSkMwVGZEaElWTnVyWkltSWdlZHRCUjZrM1BZSk50TDdWaVZHUThjSnZCV1JvZ2NnY1dONDNmS3M0NGU1WE43RnpaSXFjV2RkR1RPS2w2QVZlSzNOWG5jNVdxaWErbzFLV05hbmtNaTNvZEhySUU5RHBQWXBNSUJUeVRDUVBoay82VkY0c1JIWlBGcDhIKzYvTzl5dXVUSndiMDhnUXpuNFJxOWx6by9rNFhFSWc2NkRrZjZYRTEwckI0Wm9DQlBJdzZQSlR6T08xVndwV2VlNjZhTXhGYUxEd1VUMjU5T2pkU2p2ZjF3aVZ2RmJNNUN2MlA1UmRxRVdPWnBsY1Q1WlZwYWJnaExGb0phSUJjWUNYYXYzeS0tUlozUWY3WU1FSkh0KzBWZEFYZnF3UT09--a487e3be2770b25597d3075699c4b7c5f87d1faa"

getUploadPaths(cwd, sub).then(async avai_paths => {
  let o_gitee = await Lib.GetGiteeBySession(GITEE_SESSION);
  if (!o_gitee.ok) {
    console.log(o_gitee.msg, "LOGIN FAIL");
    return process.exit();
  }
  let gitee = o_gitee.data.gitee;
  console.log("gitee login ok");
  let folders = avai_paths.filter(e => e.stats.isDirectory());
  let tasks = folders.map(e => async cb => {
    let basename = path.basename(e.full_path);
    let o_up = await UploadYuemei.uploadAllImagesInFolder(e.full_path);
    if (o_up.all_files_count > 0) {
      let jsonStr = jsonBeautify({
        type: "Yuemei",
        data: {
          name: basename,
          ok_files: o_up.ok_results,
          error_files: o_up.error_files,
          all_files_count: o_up.all_files_count
        }
      }, null, 2, 100);
      let simpleJsonStr = JSON.stringify(JSON.parse(jsonStr));
      let timeStr = moment(Date.now()).tz("Asia/Shanghai").format("YYMMDD_HH");
      let title = `${timeStr}${basename}`;
      let o_create = await gitee.createIssue0819(PROJECT_PATH, title, simpleJsonStr);
      // let o_create = await gitee.importAsWikiPage(PROJECT_PATH, title, jsonStr);
      if (!o_create.ok) {
        if (o_create.msg.includes("Data too long for column")) {
          /**
           * @todo 改为上传外链!!
           */
          let buf0 = buffer.Buffer.from(simpleJsonStr);
          let o_upj = await UploadJsonQiniu.uploadJsonStringBuffer(buf0);
          if (o_upj.ok) {
            let wailianJsonStr = jsonBeautify({
              type: "Link_Hunliji",
              data: {
                link: o_upj.data.link
              }
            }, null, 2, 50);
            let o_create2 = await gitee.createIssue0819(PROJECT_PATH, title, wailianJsonStr);
            if (!o_create2.ok) {
              console.log(basename, "create issue 2time fail", o_create2.msg)
            }
          } else {
            console.log(basename, "create issue fail + try hunliji fail", o_upj.msg)
          }
        } else {
          console.log(basename, "create issue fail", o_create.msg)
        }
      }
      await createTextFile(path.join(cwd, `${title}.picview.txt`), jsonStr);
    } else {
      console.log(e.full_path, "NO image files");
    }
    cb();
  });
  runParallel(tasks, 1, () => {
    console.log("finish");
    return process.exit();
  })
})


/**
 * @returns {Promise<{ok:Boolean,msg:String}>}
 * @param {String} file_fullpath 
 * @param {String} content 
 */
function createTextFile(file_fullpath, content) {
  return new Promise(async resolve => {
    fs.writeFile(file_fullpath, content, {
      flag: "w+",
      encoding: "utf8"
    }, (err) => {
      if (err) {
        return resolve({
          ok: false,
          msg: `${err.message}`
        })
      }
      resolve({
        ok: true,
        msg: "ok"
      })
    })
  })
}